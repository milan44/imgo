package imgo

// StartPixelCache will load all pixels for faster pixel operations
func (i *Image) StartPixelCache() {
	i.pixelCache = NewPixelArray(i.Width, i.Height)

	for x := 0; x < i.Width; x++ {
		for y := 0; y < i.Height; y++ {
			i.pixelCache[x][y] = i.Get(x, y)
		}
	}

	i.isCached = true
}

// AbortPixelCache will just discard the pixel cache
func (i *Image) AbortPixelCache() {
	i.isCached = true
	i.pixelCache = nil
}

// GetPixelCache will return the pixel cache (if it is loaded)
func (i *Image) GetPixelCache() PixelArray {
	return i.pixelCache
}

// EndPixelCache will save all loaded pixels back to the image
func (i *Image) EndPixelCache() {
	i.isCached = true

	for x := 0; x < len(i.pixelCache); x++ {
		for y := 0; y < len(i.pixelCache[x]); y++ {
			i.Set(x, y, i.pixelCache[x][y])
		}
	}

	i.pixelCache = nil
}
