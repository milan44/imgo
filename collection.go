package imgo

/*
 *  0 | 3 | 5
 * ---+---+---
 *  1 | x | 6
 * ---+---+---
 *  2 | 4 | 7
 */

// SurroundingPixels is a slice of pixels which surround another pixel
type SurroundingPixels []Pixel

// Avg returns the average of all the pixels
func (p SurroundingPixels) Avg() Pixel {
	var r, g, b, a int
	l := len(p)

	if l == 1 {
		return p[0]
	} else if l == 0 {
		return emptyPixel
	}

	for _, pixel := range p {
		r += int(pixel.R)
		g += int(pixel.G)
		b += int(pixel.B)
		a += int(pixel.A)
	}

	return Pixel{
		R: uint8(r/l),
		G: uint8(g/l),
		B: uint8(b/l),
		A: uint8(a/l),
	}
}



// TopLeft returns the top left pixel
func (p SurroundingPixels) TopLeft() Pixel {
	return p[0]
}

// Top returns the top pixel
func (p SurroundingPixels) Top() Pixel {
	return p[3]
}

// TopRight returns the top right pixel
func (p SurroundingPixels) TopRight() Pixel {
	return p[5]
}

// CenterLeft returns the center left pixel
func (p SurroundingPixels) CenterLeft() Pixel {
	return p[1]
}

// CenterRight returns the center right pixel
func (p SurroundingPixels) CenterRight() Pixel {
	return p[6]
}

// BottomLeft returns the bottom left pixel
func (p SurroundingPixels) BottomLeft() Pixel {
	return p[2]
}

// Bottom returns the bottom pixel
func (p SurroundingPixels) Bottom() Pixel {
	return p[4]
}

// BottomRight returns the bottom right pixel
func (p SurroundingPixels) BottomRight() Pixel {
	return p[7]
}