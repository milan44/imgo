package imgo

import (
	"gonum.org/v1/gonum/mat"
	"math"
	"sync"
)

type FilterFunc func(Pixel, int, int, *Image) Pixel

// ApplyFilter runs a filter function for all pixels of the image
func (i *Image) ApplyFilter(filter FilterFunc) {
	var wg sync.WaitGroup

	for x := 0; x < i.Width; x++ {
		wg.Add(1)
		go func(x int) {
			for y := 0; y < i.Height; y++ {
				i.Set(x, y, filter(i.Get(x, y), x, y, i))
			}
			wg.Done()
		}(x)
	}

	wg.Wait()
}

func (i *Image) ApplyEdgeDetectionFilter() {
	i.ApplyFilter(FilterBaW)

	kernelx := mat.NewDense(3, 3, []float64{
		1, 0, 1,
		-2, 0, 2,
		-1, 0, 1,
	})
	kernely := mat.NewDense(3, 3, []float64{
		-1, -2, -1,
		0, 0, 0,
		1, 2, 1,
	})

	newImage := NewBlankImage(i.Width, i.Height)

	for x := 1; x < i.Width-1; x++ {
		for y := 1; y < i.Height-1; y++ {
			var magx, magy int

			for a := 0; a < 3; a++ {
				for b := 0; b < 3; b++ {
					xn := x + a - 1
					yn := y + b - 1
					magx += int(i.Get(xn, yn).R) * int(kernelx.At(a, b))
					magy += int(i.Get(xn, yn).R) * int(kernely.At(a, b))
				}
			}

			p := int(math.Sqrt(float64(magx*magx + magy*magy)))

			newImage.Set(x, y, Pixel{
				R: uint8(p),
				G: uint8(p),
				B: uint8(p),
				A: 255,
			})
		}
	}

	i.img = newImage.img
}

// FilterBaW returns the black and white representation of a pixel
func FilterBaW(p Pixel, _, _ int, _ *Image) Pixel {
	y := uint8(0.299*float64(p.R) + 0.587*float64(p.G) + 0.114*float64(p.B))
	return Pixel{
		R: y,
		G: y,
		B: y,
		A: p.A,
	}
}

// FilterInvert inverts a pixels color (not alpha)
func FilterInvert(p Pixel, _, _ int, _ *Image) Pixel {
	return Pixel{
		R: 255 - p.R,
		G: 255 - p.G,
		B: 255 - p.B,
		A: p.A,
	}
}
