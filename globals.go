package imgo

import (
	"image/gif"
	"image/jpeg"
	"image/png"
)

var (
	JPGOptions = &jpeg.Options{
		Quality: 90,
	}
	PNGEncoder = png.Encoder{
		CompressionLevel: png.NoCompression,
	}
	GIFOptions = &gif.Options{
		NumColors: 256,
	}
)

const (
	FileTypePNG = "png"
	FileTypeJPG = "jpg"
	FileTypeGIF = "gif"
)

// GetFileTypes returns all valid file types
func GetFileTypes() []string {
	return []string {
		FileTypePNG,
		FileTypeJPG,
		FileTypeGIF,
	}
}