package imgo

import (
	"bytes"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"os"
	"sync"
)

// Image the main Image object
type Image struct {
	Width  int
	Height int

	pixelCache PixelArray
	isCached   bool

	imageMutex sync.Mutex

	img *image.RGBA
}

// NewBlankImage creates a new completely blank image with the width and height given
func NewBlankImage(width, height int) Image {
	topLeft := image.Point{0, 0}
	bottomRight := image.Point{width, height}

	return Image{
		Width:  width,
		Height: height,
		img:    image.NewRGBA(image.Rectangle{topLeft, bottomRight}),
	}
}

// NewImage creates an image from a go image.Image
func NewImage(img image.Image) *Image {
	if i, ok := img.(*image.RGBA); ok {
		b := i.Bounds()

		return &Image{
			Width:  b.Max.X,
			Height: b.Max.Y,
			img:    i,
		}
	}

	return nil
}

// NewImageFromPixelArray creates a new image from a PixelArray
func NewImageFromPixelArray(p PixelArray) Image {
	width := len(p)
	height := len(p[0])

	topLeft := image.Point{0, 0}
	bottomRight := image.Point{width, height}

	img := Image{
		Width:  width,
		Height: height,
		img:    image.NewRGBA(image.Rectangle{topLeft, bottomRight}),
	}

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.Set(x, y, p.Get(x, y))
		}
	}

	return img
}

// NewImageFromFile loads an image from a file
func NewImageFromFile(file string) (*Image, error) {
	return loadImage(file)
}

func (i Image) RGBA() image.RGBA {
	return *i.img
}

func (i *Image) Clone() Image {
	newImage := NewBlankImage(i.Width, i.Height)

	newImage.ApplyFilter(func(_ Pixel, x, y int, _ *Image) Pixel {
		return i.Get(x, y)
	})

	return newImage
}

// Verify verifies that given x and y coordinates are inside the bounds of the image
func (i *Image) Verify(x, y int) bool {
	if x >= i.Width || x < 0 || y >= i.Height || y < 0 {
		return false
	}

	return true
}

// Set sets a pixel's value at a given x and y coordinate
func (i *Image) Set(x, y int, pixel Pixel) {
	if !i.Verify(x, y) {
		return
	}

	i.imageMutex.Lock()
	if i.isCached {
		i.pixelCache[x][y] = pixel
	} else {
		i.img.Set(x, y, pixel.ToColor())
	}
	i.imageMutex.Unlock()
}

// Get returns a pixel's value at a given x and y coordinate
func (i *Image) Get(x, y int) Pixel {
	if !i.Verify(x, y) {
		return Pixel{}
	}
	var pix Pixel

	i.imageMutex.Lock()
	if i.isCached {
		pix = i.pixelCache[x][y]
	} else {
		pix = PixelFromColor(i.img.At(x, y))
	}
	i.imageMutex.Unlock()
	return pix
}

// GetSurrounding returns all surrounding pixels of a given x and y coordinate
func (i *Image) GetSurrounding(x, y int) SurroundingPixels {
	px := make(SurroundingPixels, 0)
	for xo := -1; xo < 2; xo++ {
		for yo := -1; yo < 2; yo++ {
			if yo != 0 && xo != 0 {
				px = append(px, i.Get(x+xo, y+yo))
			}
		}
	}
	return px
}

// SetAlpha sets the alpha channel for all pixels
func (i *Image) SetAlpha(a uint8) {
	i.ApplyFilter(func(p Pixel, _, _ int, _ *Image) Pixel {
		p.A = a
		return p
	})
}

// SaveToFile saves the image to a file
func (i *Image) SaveToFile(file, filetype string) error {
	out, err := os.Create(file)
	if err != nil {
		return err
	}
	defer out.Close()

	return encodeImage(i.img, out, filetype)
}

func (i *Image) SaveToBytes(filetype string) ([]byte, error) {
	buf := &bytes.Buffer{}
	err := errors.New("unknown filetype")

	if filetype == FileTypePNG {
		err = PNGEncoder.Encode(buf, i.img)
	} else if filetype == FileTypeJPG {
		err = jpeg.Encode(buf, i.img, JPGOptions)
	} else if filetype == FileTypeGIF {
		err = gif.Encode(buf, i.img, GIFOptions)
	}

	return buf.Bytes(), err
}
