package imgo

import (
	"os"
	"testing"
)

func TestGetSetSaveLoadBlank(t *testing.T) {
	blank := NewBlankImage(1, 2)

	blank.Set(0, 0, testPixel1)
	blank.Set(0, 1, testPixel2)

	// Values should be same as the ones we set
	_PixelMustBe(blank.Get(0, 0), testPixel1, t, "first-blank")
	_PixelMustBe(blank.Get(0, 1), testPixel2, t, "second-blank")

	// Should return empty pixel as y value out of range
	_PixelMustBe(blank.Get(0, 34), emptyPixel, t, "invalid-y-positive")
	_PixelMustBe(blank.Get(0, -32), emptyPixel, t, "invalid-y-negative")

	// Should return empty pixel as x value out of range
	_PixelMustBe(blank.Get(34, 0), emptyPixel, t, "invalid-x-positive")
	_PixelMustBe(blank.Get(-32, 0), emptyPixel, t, "invalid-x-negative")

	// Should save image to example.png
	err := blank.SaveToFile(testFile, FileTypePNG)
	_Must(err, t, "save-blank")

	// Should load image from example.png
	loaded, err := NewImageFromFile(testFile)
	_Must(err, t, "load-img")

	// Should be same as values we set earlier
	_PixelMustBe(loaded.Get(0, 0), testPixel1, t, "first-loaded")
	_PixelMustBe(loaded.Get(0, 1), testPixel2, t, "second-loaded")

	// Get rid of our test image as we don't need it
	_ = os.Remove(testFile)
}

func TestInvertFilter(t *testing.T) {
	blank := NewBlankImage(1, 1)

	blank.Set(0, 0, redPixel)

	blank.ApplyFilter(FilterInvert)

	// Pixel should be inverted
	_PixelMustBe(blank.Get(0, 0), redInvertedPixel, t, "red-inverted")
}

func TestBaWFilter(t *testing.T) {
	blank := NewBlankImage(1, 1)

	blank.Set(0, 0, testPixel1)

	blank.ApplyFilter(FilterBaW)

	// Pixel should be black and white
	_PixelMustBe(blank.Get(0, 0), testPixel1BaW, t, "pixel-baw")
}

func TestVariousFilters(t *testing.T) {
	img, err := NewImageFromFile("test_images/flower.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	baw := img.Clone()
	baw.ApplyFilter(FilterBaW)

	err = baw.SaveToFile("baw.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	inv := img.Clone()
	inv.ApplyFilter(FilterInvert)

	err = inv.SaveToFile("inv.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	edge := img.Clone()
	edge.ApplyEdgeDetectionFilter()

	err = edge.SaveToFile("edge.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
}

func TestBasicRotation(t *testing.T) {
	img, err := NewImageFromFile("test_images/cats.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	img.BasicRotate(50, true)

	err = img.SaveToFile("output.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
}

func TestThreeShearRotate(t *testing.T) {
	img, err := NewImageFromFile("test_images/cats.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	img.ThreeShearRotate(50, true)

	err = img.SaveToFile("output.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
}

func TestNNResize(t *testing.T) {
	img, err := NewImageFromFile("test_images/cats.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	img.NNResize(500, 300)

	err = img.SaveToFile("output.png", FileTypePNG)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
}

func TestMakeThumbnail(t *testing.T) {
	img, err := NewImageFromFile("test_images/flower.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	img.MakeThumbnail(400, ThumbnailNearestNeighbor)

	if img.Width != 400 {
		t.Errorf("Width is %v not 400", img.Width)
		t.Fail()
	}

	img, err = NewImageFromFile("test_images/cats.png")
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	img.MakeThumbnail(400, ThumbnailNearestNeighbor)

	if img.Height != 400 {
		t.Errorf("Height is %v not 400", img.Height)
		t.Fail()
	}
}
