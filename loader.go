package imgo

import (
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"os"
)

func loadImage(file string) (*Image, error) {
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
	image.RegisterFormat("jpg", "jpg", jpeg.Decode, jpeg.DecodeConfig)
	image.RegisterFormat("gif", "gif", gif.Decode, gif.DecodeConfig)

	reader, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer reader.Close()

	img, _, err := image.Decode(reader)
	if err != nil {
		return nil, err
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	i := NewBlankImage(width, height)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pixel := PixelFromColor(img.At(x, y))
			i.Set(x, y, pixel)
		}
	}

	return &i, nil
}

func encodeImage(img image.Image, out *os.File, filetype string) error {
	if filetype == FileTypePNG {
		return PNGEncoder.Encode(out, img)
	} else if filetype == FileTypeJPG {
		return jpeg.Encode(out, img, JPGOptions)
	} else if filetype == FileTypeGIF {
		return gif.Encode(out, img, GIFOptions)
	}

	return errors.New("unknown filetype")
}