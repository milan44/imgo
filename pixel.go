package imgo

import (
	"fmt"
	"image/color"
)

// Pixel defines the colors of a specific pixel
type Pixel struct {
	R uint8
	G uint8
	B uint8
	A uint8
}

func rgbaToPixel(r uint32, g uint32, b uint32, a uint32) Pixel {
	return Pixel{uint8(r / 257), uint8(g / 257), uint8(b / 257), uint8(a / 257)}
}

// PixelFromColor creates a Pixel from a color.Color
func PixelFromColor(c color.Color) Pixel {
	return rgbaToPixel(c.RGBA())
}

// ToColor returns the color.Color representation of the pixel
func (p Pixel) ToColor() color.Color {
	return color.RGBA{
		R: p.R,
		G: p.G,
		B: p.B,
		A: p.A,
	}
}

// String returns a string representation of the Pixel
func (p Pixel) String() string {
	return fmt.Sprint("Pixel{", p.R, p.G, p.B, p.A, "}")
}

// IsEmpty returns true if the pixel's r,g,b and a are all 0
func (p Pixel) IsEmpty() bool {
	return p.Compare(emptyPixel)
}

// Compare compares the pixel with another one
func (p Pixel) Compare(p1 Pixel) bool {
	return p.R == p1.R && p.G == p1.G && p.B == p1.B && p.A == p1.A
}
