package imgo

// PixelArray is a 2 dimensional array of pixels
type PixelArray [][]Pixel

// NewPixelArray returns a new PixelArray with the given width and height
func NewPixelArray(width, height int) PixelArray {
	p := make(PixelArray, width)

	for i, _ := range p {
		p[i] = make([]Pixel, height)
	}

	return p
}

// Set sets a pixel's value at a given x and y coordinate
func (p *PixelArray) Set(x, y int, pixel Pixel) {
	if !p.Verify(x, y) {
		return
	}
	(*p)[x][y] = pixel
}

// Get returns a pixel's value at a given x and y coordinate
func (p *PixelArray) Get(x, y int) Pixel {
	if !p.Verify(x, y) {
		return Pixel{}
	}

	return (*p)[x][y]
}

// GetSurrounding returns all surrounding pixels of a given x and y coordinate
func (p *PixelArray) GetSurrounding(x, y int) SurroundingPixels {
	px := make(SurroundingPixels, 0)
	for xo := -1; xo < 2; xo++ {
		for yo := -1; yo < 2; yo++ {
			if yo != 0 && xo != 0 {
				px = append(px, p.Get(x+xo, y+yo))
			}
		}
	}
	return px
}

// Verify verifies that given x and y coordinates are inside the bounds of the PixelArray
func (p *PixelArray) Verify(x, y int) bool {
	if x < 0 || x >= len(*p) || y < 0 || y >= len((*p)[0]) {
		return false
	}

	if len((*p)[x]) == 0 {
		(*p)[x] = make([]Pixel, len((*p)[0]))
	}

	return true
}
