package imgo

import "testing"

var (
	testPixel1 = Pixel{100, 50, 231, 255}
	testPixel2 = Pixel{93, 146, 23, 255}

	testPixel1BaW = Pixel{85, 85, 85, 255}

	redPixel         = Pixel{255, 0, 0, 255}
	redInvertedPixel = Pixel{0, 255, 255, 255}

	emptyPixel = Pixel{0, 0, 0, 0}

	testFile = "example.png"
)

func _PixelMustBe(p, p2 Pixel, t *testing.T, def string) {
	comp := p.Compare(p2)

	if !comp {
		t.Error("Should be " + p2.String() + " but is " + p.String() + " [" + def + "]")
		t.Fail()
	}
}

func _Must(err error, t *testing.T, def string) {
	if err != nil {
		t.Error(err.Error() + " [" + def + "]")
		t.Fail()
	}
}
