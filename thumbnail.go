package imgo

const (
	ThumbnailNearestNeighbor = 0
)

// MakeThumbnail makes sure the images height and width don't exceed the given maxSize while keeping the aspect ratio using the given thumbnailFunction
func (i *Image) MakeThumbnail(maxSize, thumbnailFunction int) {
	if maxSize <= 0 || (i.Height <= maxSize && i.Width <= maxSize) {
		return
	}

	var h, w int

	if i.Height > i.Width {
		h = maxSize
	} else {
		w = maxSize
	}

	switch thumbnailFunction {
	case ThumbnailNearestNeighbor:
		i.NNResize(w, h)
	}
}
