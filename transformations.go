package imgo

import (
	"gonum.org/v1/gonum/mat"
	"math"
	"sync"
)

// RotateLeft90 rotates the image counter-clockwise by 90 degrees
func (i *Image) RotateLeft90() {
	ni := NewBlankImage(i.Height, i.Width)

	for y := 0; y < i.Height; y++ {
		for x := 0; x < i.Width; x++ {
			nx := y
			ny := i.Width - x

			ni.Set(nx, ny, i.Get(x, y))
		}
	}

	i.img = ni.img
	i.Width = ni.Width
	i.Height = ni.Height
}

// RotateRight90 rotates the image clockwise by 90 degrees
func (i *Image) RotateRight90() {
	ni := NewBlankImage(i.Height, i.Width)

	for y := 0; y < i.Height; y++ {
		for x := 0; x < i.Width; x++ {
			nx := i.Height - y
			ny := x

			ni.Set(nx, ny, i.Get(x, y))
		}
	}

	i.img = ni.img
	i.Width = ni.Width
	i.Height = ni.Height
}

// BasicRotate performs a basic rotation on the image
func (i *Image) BasicRotate(angle float64, doCleanup bool) {
	angle = angle * math.Pi / 180

	cos := math.Cos(angle)
	sin := math.Sin(angle)

	matrices := []*mat.Dense{
		mat.NewDense(2, 2, []float64{
			cos, -sin,
			sin, cos,
		}),
	}

	i.ApplyTransformationMatrices(doCleanup, matrices...)
}

// ThreeShearRotate performs a three shear rotate on the image
func (i *Image) ThreeShearRotate(angle float64, doCleanup bool) {
	angle = angle * math.Pi / 180

	tan := math.Tan(angle/2) * -1
	sin := math.Sin(angle)

	matrices := []*mat.Dense{
		mat.NewDense(2, 2, []float64{
			1, tan,
			0, 1,
		}),
		mat.NewDense(2, 2, []float64{
			1, 0,
			sin, 1,
		}),
		mat.NewDense(2, 2, []float64{
			1, tan,
			0, 1,
		}),
	}

	i.ApplyTransformationMatrices(doCleanup, matrices...)
}

// ApplyTransformationMatrices applies given transformation matrices to the image
func (i *Image) ApplyTransformationMatrices(doCleanup bool, matrices ...*mat.Dense) {
	if len(matrices) == 0 {
		return
	}

	newImage := NewBlankImage(i.Width, i.Height)

	var wg sync.WaitGroup

	for y := 0; y < i.Height; y++ {
		wg.Add(1)
		go func(y int) {
			for x := 0; x < i.Width; x++ {
				tx := x - (i.Width / 2)
				ty := y - (i.Height / 2)

				pos := mat.NewDense(2, 1, []float64{
					float64(tx),
					float64(ty),
				})

				var nx, ny float64

				for _, matrix := range matrices {
					var final mat.Dense
					final.Mul(matrix, pos)

					nx = final.At(0, 0)
					ny = final.At(1, 0)

					pos = mat.NewDense(2, 1, []float64{
						nx,
						ny,
					})
				}

				nxi := int(nx) + (i.Width / 2)
				nyi := int(ny) + (i.Height / 2)
				newImage.Set(nxi, nyi, i.Get(x, y))
			}

			wg.Done()
		}(y)
	}

	wg.Wait()

	if doCleanup {
		newImage._matrixCleanup()
	}

	i.img = newImage.img
}

func (i *Image) _matrixCleanup() {
	i.ApplyFilter(func(pix Pixel, x, y int, i *Image) Pixel {
		if pix.IsEmpty() {
			previous := i.Get(x-1, y)
			next := i.Get(x+1, y)

			if !previous.IsEmpty() && !next.IsEmpty() {
				return previous
			} else if x == 0 && !next.IsEmpty() {
				return next
			}
		}

		return pix
	})
}

// NNResize does a Nearest neighbor resize
func (i *Image) NNResize(width, height int) {
	if width <= 0 && height <= 0 {
		return
	} else if width <= 0 {
		hf := float64(i.Height) / float64(height)
		width = int(math.Round(float64(i.Width) / hf))
	} else if height <= 0 {
		wf := float64(i.Width) / float64(width)
		height = int(math.Round(float64(i.Height) / wf))
	}

	heightFactor := float64(i.Height) / float64(height)
	widthFactor := float64(i.Width) / float64(width)

	newImage := NewBlankImage(width, height)

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			ox := int(math.Round(float64(x) * widthFactor))
			oy := int(math.Round(float64(y) * heightFactor))

			newImage.Set(x, y, i.Get(ox, oy))
		}
	}

	i.img = newImage.img
	i.Height = newImage.Height
	i.Width = newImage.Width
}
